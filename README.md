# README #

Despite the mildly confusing name of the repository, this is for the grade calculator, not anything pokemon related.
Was considering making some pokemon project, and chose against it.
### What is this repository for? ###

Has all the files for a grade calculator using Qt.

### How do I get set up? ###

Once all the files are in Qt, it should run fine. The calculator has two tabs, one for Pic10b and the other for Pic10c. 
Input the scores into the sliders or spinboxes and then choose a grading scheme by pressing the push buttons. The scores
will be outputted where labelled.


