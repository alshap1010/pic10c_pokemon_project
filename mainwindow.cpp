#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->PushforB_A,SIGNAL(clicked()),
                        this,SLOT(GradeCalculationBSchemeA()));
    QObject::connect(ui->PushforB_B,SIGNAL(clicked()),
                        this,SLOT(GradeCalculationBSchemeB()));
    QObject::connect(ui->PushforC_A,SIGNAL(clicked()),
                        this,SLOT(GradeCalculationCSchemeA()));
    QObject::connect(ui->PushforC_B,SIGNAL(clicked()),
                        this,SLOT(GradeCalculationCSchemeB()));
}

void MainWindow::GradeCalculationBSchemeA(){
   double hw = (ui->BHW1Spin->value() + ui->BHW2Spin->value()+ui->BHW3Spin->value() +ui->BHW4Spin->value()
                +ui->BHW5Spin->value() +ui->BHW6Spin->value()+ui->BHW7Spin->value() +ui->BHW8Spin->value()) / 800.0;
   double mid = (ui->BMID1Slide->value() + ui->BMID2Slide->value()) / 200.0;
   double final = static_cast<double>(ui->BFINALSlide->value()) / 100;
   double final_score = 25*hw + 40*mid + 35*final;
   ui->Grade_Score_B->setText(QString::number(final_score));

}

void MainWindow::GradeCalculationBSchemeB(){
   double hw = (ui->BHW1Spin->value() + ui->BHW2Spin->value()+ui->BHW3Spin->value() +ui->BHW4Spin->value()
                +ui->BHW5Spin->value() +ui->BHW6Spin->value()+ui->BHW7Spin->value() +ui->BHW8Spin->value()) / 800.0;

   double mid =ui->BMID1Slide->value() / 100.0;
   if(ui->BMID1Slide->value() < ui->BMID2Slide->value())
       mid = ui->BMID2Slide->value() / 100.0;

   double final = static_cast<double>(ui->BFINALSlide->value()) / 100.0;
   double final_score = 25*hw + 30*mid + 44*final;
   ui->Grade_Score_B->setText(QString::number(final_score));
}

void MainWindow::GradeCalculationCSchemeA(){
   double hw = (ui->CHW1Spin->value() + ui->CHW2Spin->value()+ui->CHW3Spin->value()) / 300.0;
   double mid = ui->CMIDSlide->value() / 100.0;
   double project =  ui->CPROJECTSlide->value() / 100.0;
   double final = static_cast<double>(ui->CFINALSlide->value()) / 100;
   double final_score = 15*hw + 25*mid + 35*project + 25*final;
   ui->Grade_Score_C->setText(QString::number(final_score));

}

void MainWindow::GradeCalculationCSchemeB(){
    double hw = (ui->CHW1Spin->value() + ui->CHW2Spin->value()+ui->CHW3Spin->value()) / 300.0;
    double project =  ui->CPROJECTSlide->value() / 100.0;
    double final = static_cast<double>(ui->CFINALSlide->value()) / 100;
    double final_score = 15*hw + 35*project + 50*final;
   ui->Grade_Score_C->setText(QString::number(final_score));
}

MainWindow::~MainWindow()
{
    delete ui;
}
